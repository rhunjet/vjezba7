﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proba {
    class Racun<T> : IRacun<T>{
        public Racun(T iznosRacuna) {
            IznosRacuna = iznosRacuna;
            
        }
        public DateTime DatumIzdavanja { get; set; }
        public T IznosRacuna {
            set {
                if(Convert.ToDouble(value) < 10) {
                    throw new IznimkaBlagajne("Premali iznos!");
                }
                IznosRacuna = value;
            }

            get {
                return IznosRacuna;
            }
        }
        public decimal DohvatiIznos() {

            return Convert.ToDecimal(IznosRacuna);
        }

        public DateTime DohvatiDatumIzdavanja() {
            return DateTime.Now;
        }


    }
     



}
