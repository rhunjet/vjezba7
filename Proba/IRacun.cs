﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proba {
    interface IRacun<T> {
        decimal DohvatiIznos();
        DateTime DohvatiDatumIzdavanja();
    }
}
